## Methods & Operations
This crate will try to add as many methods as users will used as well as making it easy to interact with user-defined
methods. Never the less it will start with the ones that we consider most known/used by the community. 
- [ ] `did-erc725`
- [ ] `did-btcr`
- [ ] `did-sov`
- [ ] Open an issue if you consider an important method has to be implemented.

Following the w3's description:

> To enable the full functionality of DIDs and DID Documents on a particular Decentralized Identifier Registry, a DID 
method specification MUST specify how each of the following CRUD operations is performed by a client. 
> Each operation MUST be specified to the level of detail necessary to build and test interoperable client 
implementations with the target system. 
> Note that, due to the specified contents of DID Documents, these operations can effectively be used to 
perform all the operations required of a CKMS (cryptographic key management system).

