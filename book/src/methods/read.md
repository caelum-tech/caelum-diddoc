## Read/Verify
**Not implemented.**
> The DID method specification MUST specify how a client uses a DID to request a DID Document from the Decentralized 
Identifier Registry, including how the client can verify the authenticity of the response.
