//!
//! Definition of main functionalities (properties with examples, getters, adders, setters)
//!
//!
use serde::Deserialize;
use serde::Serialize;

use wasm_bindgen::prelude::*;

use crate::did_doc_authentication::Authentication;
use crate::did_doc_public_key::DidDocPublicKey;

use crate::proof::Proof;
use crate::service::Service;

#[derive(Serialize, Deserialize, Debug)]
pub struct DidDocument {
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "@context", deserialize = "@context"))]
    pub context: Vec<String>,
    #[serde(default = "empty_string")]
    pub id: String,
    #[serde(default = "Vec::new")]
    pub authentication: Vec<Authentication>,
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "publicKey", deserialize = "publicKey"))]
    pub public_key: Vec<DidDocPublicKey>,
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "service", deserialize = "service"))]
    pub service: Vec<Service>,
    #[serde(default = "Proof::default")]
    pub proof: Proof,
    #[serde(default = "empty_string")]
    pub created: String,
    #[serde(default = "empty_string")]
    pub updated: String,
}

fn empty_string() -> String {
    "".to_string()
}

impl DidDocument {
    // CONSTRUCTORS
    /// # DidDocument Constructor
    /// `DidDoc`'s constructor will create an empty `DidDoc` object. An empty object
    /// is not a valid `DidDoc`.
    /// ```rust
    /// use caelum_diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// ```
    pub fn new(context: String, id: String) -> Self {
        DidDocument {
            context: vec![context],
            id,
            authentication: Vec::new(),
            public_key: Vec::new(),
            service: Vec::new(),
            proof: Proof::default(),
            created: "".to_string(),
            updated: "".to_string(),
        }
    }

    // GENERATORS
    /// # Generate `String` from Did Document
    /// ```javascript
    /// let my_string = dd.to_str();
    /// ```
    ///
    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    /// # Generate `JsValue` from Did Document
    /// ```javascript
    /// let my_json = dd.toJSON();
    /// ```
    ///
    pub fn to_json(&self) -> JsValue {
        JsValue::from_serde(&self).unwrap()
    }
}

impl Default for DidDocument {
    fn default() -> Self {
        Self {
            context: vec!["https://www.w3.org/ns/did/v1".to_string()],
            id: "".to_string(),
            authentication: Vec::new(),
            public_key: Vec::new(),
            service: Vec::new(),
            proof: Proof::default(),
            created: "".to_string(),
            updated: "".to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_diddoc() {
        println!("HEllooooo");
        let mut should = DidDocument::new("my_context".to_string(), "my_id".to_string());
        should.proof = serde_json::from_str(
            r#"
          {
            "type": "RsaSignature2018",
            "created": "2018-06-17T10:03:48Z",
            "creator": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
            "signatureValue": "pY9...Cky6Ed = "
          }"#,
        )
        .unwrap();

        let dd_auth: DidDocument = serde_json::from_str(
            &r#"
        {
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [],
            "publicKey": [],
            "service": [],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "creator": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "",
            "updated": ""
        }
        "#,
        )
        .unwrap();

        assert_eq!(
            serde_json::to_string(&should).unwrap(),
            serde_json::to_string(&dd_auth).unwrap()
        );
    }
}
