use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Proof {
    #[serde(rename(serialize = "type", deserialize = "type"))]
    auth_type: String,
    created: String,
    creator: String,
    #[serde(rename(serialize = "signatureValue", deserialize = "signatureValue"))]
    signature_value: String,
}

impl Proof {
    pub fn new(
        auth_type: String,
        created: String,
        creator: String,
        signature_value: String,
    ) -> Self {
        Proof {
            auth_type,
            created,
            creator,
            signature_value,
        }
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}

impl Default for Proof {
    fn default() -> Self {
        Proof {
            auth_type: "".to_string(),
            created: "".to_string(),
            creator: "".to_string(),
            signature_value: "".to_string(),
        }
    }
}
