extern crate caelum_diddoc;

use caelum_diddoc::did_doc_authentication::Authentication;
use caelum_diddoc::did_doc_public_key::DidDocPublicKey;
use caelum_diddoc::did_document::DidDocument;
use caelum_diddoc::proof::Proof;
use caelum_diddoc::service::Service;

#[test]
fn didddoc_construction() {
    let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    dd.authentication = vec![Authentication::PublicKey(
        serde_json::from_str(
            r#"{
                "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                "type": "OpenIdConnectVersion1.0Service",
                "controller": "CONTROLLER!",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    }"#,
        )
        .unwrap(),
    )];
    dd.authentication.push(Authentication::DID(
        "did:lrn:123412341234#my_id".to_string(),
    ));
    // Add first public key
    dd.public_key.push(DidDocPublicKey::new(
        "keys-2".to_string(),
        "Ed25519VerificationKey2018".to_string(),
        "did:example:pqrstuvwxyz0987654321".to_string(),
        "PublicKeyBase58".to_string(),
        "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV".to_string(),
    ));
    // Overwrite public key in a different way
    dd.public_key = vec![serde_json::from_str(
        r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "controller": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
      }"#,
    )
    .unwrap()];
    // Yet another way to add a public key
    dd.public_key.push(
        serde_json::from_str(
            r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "controller": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
      }"#,
        )
        .unwrap(),
    );
    // Updating a particular index in public key Vector
    dd.public_key[0] = serde_json::from_str(
        r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "controller": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
      }"#,
    )
    .unwrap();

    // Add first service
    dd.service.push(Service::new(
        "my_id".to_string(),
        "https://notary.ownyourdata.eu/?hash=fef73e273".to_string(),
        "custom".to_string(),
    ));
    // Updating a particular index in public key Vector
    dd.service[0] = serde_json::from_str(
        r#"{
        "id": "my_id",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
      }"#,
    )
    .unwrap();
    // Add another service in a different way
    dd.service.push(
        serde_json::from_str(
            r#"{
        "id": "my_id",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
      }"#,
        )
        .unwrap(),
    );
    // Overwrite service yet in another way
    dd.service = vec![serde_json::from_str(
        r#"{
        "id": "my_id",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
      }"#,
    )
    .unwrap()];
    
    // Setting a proof
    dd.proof = serde_json::from_str(
        r#"{
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "creator": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
      }"#,
    )
    .unwrap();
    // Equivalent to th following
    dd.proof = Proof::new(
        "RsaSignature2018".to_string(),
        "2018-06-17T10:03:48Z".to_string(),
        "did:example:ebfeb1276e12ec21f712ebc6f1c#k1".to_string(),
        "pY9...Cky6Ed = ".to_string(),
    );

    dd.created = "2002-10-10T17:00:00Z".to_string();
    dd.updated = "2016-10-17T02:41:00Z".to_string();
    let dd_j: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                    "type": "OpenIdConnectVersion1.0Service",
                    "controller": "CONTROLLER!",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                },
                "did:lrn:123412341234#my_id"
            ],
            "publicKey": [
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                },
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [{
                "id": "my_id",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "creator": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "2002-10-10T17:00:00Z",
            "updated": "2016-10-17T02:41:00Z"
        }"#,
    )
    .unwrap();

    assert_eq!(dd.to_str(), dd_j.to_str());
}
