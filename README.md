# **D**ecentralized **Id**entity **Doc**uments

The `diddoc` crate is aimed to help `javascript` and `rust` developers manage `DID docs` 
(**D**ecentralized **Id**entity **Doc**uments) in an easy manner.
Following [w3's specifications](https://w3c-ccg.github.io/did-spec/#did-documents) on DID
Documents this crate aims to be a [Resolver](https://w3c-ccg.github.io/did-spec/#did-resolvers).
This means it will create, read, update and delete all properties of a DID document. As well as
interacting with other DIDs and DID document, using proof verification.
Also, it will be able to interact with others on other Decentralized Identifier Registry.
This will can effectively be used to perform all the operations required of a CKMS
(cryptographic key management system), such as key registration, replacement, rotation,
recovery, and expiration.
More details in [w3's method specifications](https://w3c-ccg.github.io/did-spec/#did-methods).

## Documentation

You can read [our code documentation](https://docs.rs/crate/caelum-diddoc), which makes reference to usage with javascript and rust as well as the actual implementation of the library.

For more details you can take a look at our book.

### Book

Clone repository and go to the `book` directory:

```sh
git clone https://gitlab.com/caelum-tech/caelum-diddoc.git
cd caelum-diddoc/book
```

Download `mdbook` and build the documentation:

```sh
cargo install mdbook
mdbook build
```

Open the book `/your/relative/path/caelum-diddoc/book/book/index.html` on your favorite browser.

## Installing `DidDoc` for `JavaScript`

### From `npm`

Add it to your package.json:

```json
"devDependencies": {
    "@caelum-tech/caelum-diddoc-nodejs": "^0.1.2",
   ...
  }
```

Imported in your code:

```javascript
const diddoc = require('@caelum-tech/caelum-diddoc-nodejs')
```

Finally you can use it as any other package:

```javascript
const d = new diddoc.DidDoc('https://www.w3.org/ns/did/v1', 'did:lor:cat:lab:root')
console.log(d.toJSON())
```

### From gitlab

Clone or download `diddoc` repository

```sh
git clone ...
```

Go to `diddoc` directory and build the package using `wasm-pack` (make sure wasm-pack is installed)

```sh
cd diddoc

wasm-pack build
```

This will generate a directory called `pkg` inside `diddoc`  directory.

Add the generated package to your `package.json`;

```json
"devDependencies": {
    "diddoc": "file:../diddoc/pkg",
    ...
}
```

Now your ready to import it in your `JavaScript` code as usual:

```javascript
const diddoc = require('diddoc')
```

Finally you can use it as any other package:

```javascript
const d = new diddoc.DidDoc('https://www.w3.org/ns/did/v1', 'did:lor:cat:lab:root')
console.log(d.toJSON())
```

### From npm

Installed as a normal npm package:

```sh
npm i @caelum-tech/caelum-diddoc-nodejs
```

## Installing `DidDoc` for `Rust`

Add crate to your Cargo.toml

```toml
...
[dependencies]
caelum-diddoc = "0.1.2"
...
```

Use and instantiate:

```rust
use caelum_diddoc::DidDocument;
let dd = DidDocument::default();
println!("{:#?}", dd);
```


## Resolvers

> A DID resolver is a software component with an API designed to accept requests for DID lookups and execute the
> corresponding DID method to retrieve the authoritative DID Document. To be conformant with this specification, a DID
> resolver:
>
> SHOULD validate that a DID is valid according to its DID method specification, otherwise it should produce an error.
>
> MUST conform to the requirements of the applicable DID method specification when performing DID resolution
> operations.
>
> SHOULD offer the service of verifying the integrity of the DID Document if it is signed.
>
> MAY offer the service of returning requested properties of the DID Document.

## Roadmap

- [ x ] Basic CRUD implementation on properties.
- [ x ] Basic CRUD implementation on Optional properties.
- [ ] Basic interaction with IPFS.
- [ ] Verifying cryptographic proofs.
- [ ] Implementing important DID methods such as `did-erc725`, `did-btcr`, `did-sov`, and others.
- [ ] Helper to implement DID for own methods.

## Contributing

Please, contribute to `diddoc`! The more the better! Feel free to to open an issue and/or contacting directly with the
owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy w3's specifications.

## Code of conduct

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at
[http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License

This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in
[LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) respectively.
